from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from telegram.error import TelegramError
from pathlib import Path
import logging
import validators
import youtube_dl
import os

def download_webfile(url, filename=None, **ydl_opts):
    if filename is not None:
        ydl_opts["outtmpl"]= filename
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download(url_list=[url])

def start(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text="Hello, Please enter Valid Youtube Url:")

def url_downloader(bot, update):
    print(update.message.text)
    url = update.message.text
    if (validators.url(url)):
        # bot.send_message(chat_id=update.message.chat_id, text="Downloading please wait...")
        bot.sendMessage(chat_id=update.message.chat_id, text="Downloading Please wait ...")
        download_webfile(url=url, filename=str(update.message.chat_id))
        file_name = Path(str(update.message.chat_id))
        if (file_name.exists()):
            path = str(file_name.absolute())
            video = open(path, 'rb')
            video.seek(0)
            try:
                size = os.path.getsize(path) >> 20
                if size < 50:
                    bot.sendVideo(chat_id=update.message.chat_id, video=video)
                else:
                    bot.sendMessage(chat_id=update.message.chat_id, text="too large file")
                    bot.sendMessage(chat_id=update.message.chat_id, text="50mb telegram limit")

                video.close()
                os.remove(path)
            except Exception as e:
                print(str(e))

        else:
            bot.send_message(chat_id=update.message.chat_id, text="Cant download link")

    else:
        bot.send_message(chat_id=update.message.chat_id, text="Please Enter Valid url")

token = "434202070:AAFFZCkKeQnU-AsgvYGF3ljNr6nFVwq62bE"
updater = Updater(token=token)

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',level=logging.DEBUG)
dispatcher = updater.dispatcher

start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)

url_handler = MessageHandler(Filters.text, url_downloader)
dispatcher.add_handler(url_handler)

# updater.start_webhook(listen='0.0.0.0',
#                       port=8443,
#                       url_path='{}'.format(token),
#                       key='private.key',
#                       cert='cert.pem',
#                       webhook_url='https://5.2.76.242:5000/{}'.format(token))

# updater.start_webhook(listen="0.0.0.0",
#                       port=5000,
#                       url_path=str(token))
# updater.bot.set_webhook('https://5.2.76.242/{}'.format(token))
# updater.idle()

updater.start_polling()
updater.idle()

# updater.start_webhook(listen='0.0.0.0', port=5000, url_path=token)
# webhook_url =
# updater.bot.set_webhook(url=webhook_url ,certificate=open('/home/alireza/ssl/cert.pem', 'rb'))
